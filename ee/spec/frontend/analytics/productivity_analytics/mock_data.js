const mockMergeRequests = [
  {
    id: 34,
    iid: 10,
    description: 'some descirption goes here',
    title: 'This is a test MR',
    days_to_merge: 24,
    time_to_first_comment: 596,
    time_to_last_commit: 0,
    time_to_merge: 0,
    commits_count: 1,
    loc_per_commit: 3,
    files_touched: 1,
    author_avatar_url: null,
    merge_request_url: 'http://gitlab.example.com/gitlab-org/gitlab-test/merge_requests/10',
  },
  {
    id: 6,
    iid: 6,
    description:
      'Error temporibus odit veniam expedita ipsa eum quia et. Quo deserunt accusamus ut est ab. Quia sit delectus possimus aut odio veritatis voluptatibus ullam.',
    title: 'Vero sint consectetur velit sit totam ipsa aut omnis non repellendus.',
    days_to_merge: 139,
    time_to_first_comment: 0,
    time_to_last_commit: -4530,
    time_to_merge: 7866,
    commits_count: 3,
    loc_per_commit: 4,
    files_touched: 3,
    author_avatar_url: null,
    merge_request_url: 'http://lgitlab.example.com/gitlab-org/gitlab-test/merge_requests/6',
  },
];

export default mockMergeRequests;
